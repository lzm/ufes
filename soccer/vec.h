#ifndef VEC_H
#define VEC_H

#include <math.h>

struct vec {
   float x, y, z;
   vec(float _x=0, float _y=0, float _z=0) : x(_x), y(_y), z(_z) { }
   vec operator-(void) const { return vec(-x, -y, -z); }
   vec operator+(const vec& v) const { return vec(x+v.x, y+v.y, z+v.z); }
   vec operator-(const vec& v) const { return operator+(-v); }
   vec operator*(float m) const { return vec(x*m, y*m, z*m); }
   vec operator/(float m) const { return operator*(1/m); }
   vec& operator+=(const vec& v) { return *this = *this+v; }
   vec& operator-=(const vec& v) { return *this = *this-v; }
   vec& operator*=(float m) { return *this = *this*m; }
   vec& operator/=(float m) { return *this = *this/m; }
   float norm(void) const { return sqrtf(x*x+y*y+z*z); }
};

#endif
