#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <GL/glut.h>
#include <ctime>
#include "tga.h"
#include "vec.h"

#define foreach(x) for (typeof(x.begin()) it = x.begin(); it != x.end(); ++it)

using namespace std;

float lapse = 0;

GLuint t_skies[5], t_field;

float field_width = 50;
float field_length = 100;

GLUquadric *quad = gluNewQuadric();

struct ball_t {
   vec pos, vel;
   static const float radius = 0.5;
   static const float gravity = 0.3;

   void think()
   {
      pos += vel*lapse;

      if (pos.y != 0 && vel.y != 0)
         vel.y -= gravity;

      if (pos.y < 0) {
         pos.y *= -1;
         vel.y *= -1;
         vel *= 0.8;
      }
      if (pos.x < -field_width/2)
         vel.x *= -1;
      if (pos.x > field_width/2)
         vel.x *= -1;

      if (pos.z < -field_length/2)
         vel.z *= -1;
      if (pos.z > field_length/2)
         vel.z *= -1;
   }
   void draw()
   {
      glPushMatrix();
         glColor3f(1, 1, 1);
         glTranslatef(pos.x, pos.y+radius/2, pos.z);
         gluSphere(quad, radius, 10, 10);
      glPopMatrix();
   }
} ball;

enum team_e { RED, BLUE };

struct player
{
   vec pos;
   team_e team;
   float speed;
   float force;
   
   void think()
   {
      vec diff = ball.pos - pos;
      vec vel = diff*speed*(1/diff.norm() + sqrtf(diff.norm())*0);
      vel.y = 0;
      pos += vel*lapse;
      diff = ball.pos - pos;
      if (diff.norm() < 1) {
         if (diff.norm() < 0.1) diff = vec(100, 0, 0);
         ball.vel = (diff/(diff.norm()*diff.norm()))*force;
         ball.vel.y = force/2;
      }
   }

   void draw()
   {
      if (team == BLUE) glColor3f(0, 0, 1);
      else glColor3f(1, 0, 0);

      glPushMatrix();
         glTranslatef(pos.x, pos.y, pos.z);
         glRotatef(-90, 1, 0, 0);
         gluCylinder(quad, 1, 0.1, 2, 10, 10);
      glPopMatrix();
   }
};

struct cam_t {
   vec pos, rot;

   float view_angle()
   {
      vec diff = ball.pos - pos;
      return M_PI/2 - atan2(-diff.z, diff.x);
   }

   void rotate()
   {
      glRotatef(view_angle()*360/(2*M_PI), 0, 1, 0);
   }

   void move()
   {
      glTranslatef(-pos.x, -pos.y, -pos.z);
   }
} cam;

void draw_quad()
{
   glBegin(GL_QUADS);
      glTexCoord2f(0, 0);
      glVertex3f(-1, 0, -1);
      glTexCoord2f(1, 0);
      glVertex3f(1, 0, -1);
      glTexCoord2f(1, 1);
      glVertex3f(1, 0, 1);
      glTexCoord2f(0, 1);
      glVertex3f(-1, 0, 1);
   glEnd();
}

void draw_field()
{
   glEnable(GL_TEXTURE_2D);
   glPushMatrix();
      glScalef(field_width/2, 0, field_length/2);
      glColor3f(1, 1, 1);
      glBindTexture(GL_TEXTURE_2D, t_field);
      draw_quad();
   glPopMatrix();
   glDisable(GL_TEXTURE_2D);
}

void draw_skybox()
{
   glEnable(GL_TEXTURE_2D);
   glPushMatrix();
      glColor3f(1, 1, 1);
      glScalef(100, 100, 100);
      glRotatef(90, 1, 0, 0);

      int i;
      for (i=0; i<4; i++) {
         glPushMatrix();
            glTranslatef(0, 1, 0);
            glBindTexture(GL_TEXTURE_2D, t_skies[i]);
            draw_quad();
         glPopMatrix();
         glRotatef(90, 0, 0, 1);
      }

      glRotatef(-90, 1, 0, 0);
      glRotatef(180, 0, 1, 0);
      glTranslatef(0, 1, 0);
      glBindTexture(GL_TEXTURE_2D, t_skies[i]);
      draw_quad();

      glRotatef(90, 0, 0, 1);

   glPopMatrix();
   glDisable(GL_TEXTURE_2D);
}

vector<player> ps;

void load_game()
{
   player p;

   ps.clear();

   for (int k=0; k<2; k++) {
      p.team = k ? RED : BLUE;
      for (int i=0; i<5; i++) {
         p.pos.x = (rand()%(int)field_width) - field_width/2;
         p.pos.z = (rand()%(int)(field_length/2));
         p.speed = 3.0 + rand()%10;
         p.force = p.speed + 7.0 + rand()%15;
         if (k) p.pos.z *= -1;
         ps.push_back(p);
      }
   }

   ball.pos = vec();
   cam.pos.y = 5;
   cam.pos.x = -field_width*0.6;
}

void display(void)
{
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glLoadIdentity();

   cam.rotate();
   draw_skybox();

   glLoadIdentity();

   cam.rotate();
   cam.move();

   draw_field();

   foreach(ps) {
      (*it).think();
      (*it).draw();
   }

   ball.think();
   ball.draw();

   glutSwapBuffers();
}

void idle(void)
{
   static clock_t c = 0;
   clock_t now = clock();

   if (c) lapse = ((float)(now-c))/CLOCKS_PER_SEC;
   c = now;

   glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y)
{
   if (key == 27) exit(0);
   if (key == ' ') load_game();
   if (key == 'a') cam.pos.y += 1;
   if (key == 'z') cam.pos.y -= 1;
}

void special(int key, int x, int y)
{
   float speed = 1;
   float alpha = cam.view_angle();

   vec w(sinf(-alpha), 0, cosf(-alpha));

   w /= w.norm();
   w *= speed;

   vec a(w.z, 0, -w.x);

   if (key == GLUT_KEY_UP) cam.pos -= w;
   if (key == GLUT_KEY_DOWN) cam.pos += w;
   if (key == GLUT_KEY_LEFT) cam.pos -= a;
   if (key == GLUT_KEY_RIGHT) cam.pos += a;
}

void load_textures(void)
{
   glGenTextures(1, &t_field);
   glBindTexture(GL_TEXTURE_2D, t_field);
   load_tga("field2.tga");

   char *skyies[] = {"backsa.tga", "rightsa.tga", "frontsa.tga", "leftsa.tga", "topsa.tga"};

   for (int i=0; i<5; i++) {
      glGenTextures(1, &t_skies[i]);
      glBindTexture(GL_TEXTURE_2D, t_skies[i]);
      load_tga(skyies[i]);
   }
}

void init_gl(void)
{
   glClearColor (0.0, 0.0, 0.0, 0.0);
   glEnable(GL_DEPTH_TEST);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
}

void reshape(int w, int h)
{
   glViewport(0, 0, w, h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(45, w*1.0/(h+!h), 0.5, 500);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
}

int main(int argc, char *argv[])
{
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);
   glutInitWindowSize(640, 480);
   glutInitWindowPosition(400, 250);
   glutCreateWindow("winning twelven");
   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutKeyboardFunc(keyboard);
   glutSpecialFunc(special);
   glutIdleFunc(idle);

   srand(time(0));

   init_gl();
   load_textures();
   load_game();

   printf("keys:\n");
   printf(" esc\t- quit\n");
   printf(" space\t- restart\n");
   printf(" up\t- forward\n");
   printf(" down\t- back\n");
   printf(" left\t- left\n");
   printf(" right\t- right\n");
   printf(" a\t- up\n");
   printf(" z\t- down\n");

   glutMainLoop();

   return 0; 
}
