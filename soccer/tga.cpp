#include <assert.h>
#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include "tga.h"

void load_tga(char *file)
{
   printf("loading texture %s...\n", file);
   
   FILE *f = fopen(file, "rb");
   if (!f) perror(file), exit(0);

   struct tga_t tga;
   fread(&tga, sizeof(tga_t),1 , f);

   assert(tga.imagetype == 2);
   assert(tga.bits == 24);

   int size = tga.width * tga.height * 3;
   char *data = new char[size];

   fread(data, 1, size, f);

   // conversao bgr -> rgb
   int i, tmp;
   for (i=0; i<size; i+=3) {
      tmp = data[i];
      data[i] = data[i+2];
      data[i+2] = tmp;
   }

   fclose(f);

   glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
   glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
   gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, tga.width, tga.height, GL_RGB, GL_UNSIGNED_BYTE, data);

   delete data;
}
