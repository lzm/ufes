#ifndef RUN_H
#define RUN_H

#include "tree.h"

void eval_op1(struct opt_t *op, struct val_t *val);

void eval_op(struct opt_t *op, struct val_t *val);

void call_func(struct func_t *func, struct exp_t *exp, struct val_t *val);

void eval_func(struct call_t *call, struct val_t *val);

void eval_exp(struct exp_t *exp, struct val_t *val);

void run_stmt(struct stmt_t *stmt);

void run_stmts(struct stmt_t *stmt);

void run_block(struct block_t *block);

void run_progr(struct progr_t *progr);

#endif
