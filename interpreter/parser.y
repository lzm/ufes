%{

#include <stdarg.h>
#include <stdio.h>

#include "run.h"

#define mk(x) malloc(sizeof(struct x##_t))
#define mkop(x,y,z,w) x = mk(exp); x->type = ex_op; x->op.op = y; x->op.left = z; x->op.right = w
#define mkif(x,y,z,w) x = mk(stmt); x->type = st_if; x->if_.exp = y; x->if_.then = z; x->if_.else_ = w;

void yyerror(char *, ...);
extern FILE *yyin;
extern int yylineno;

%}

%token <id> ID
%token <num_i> INTEGER;
%token <num_d> DOUBLE;
%token <str> STRING;
%token PROGR VAR S_INTEGER S_DOUBLE S_STRING PROC FUNC BEG END
%token ATTR WHILE DO IF THEN
%nonassoc IFX
%nonassoc ELSE

%left 'o'
%left 'a'
%left '='
%left '>' '<'
%left '+' '-'
%left '*' '/' '%'
%nonassoc '!' NEG

%union {
    char *id;
    char *str;
    int num_i;
    double num_d;
    struct progr_t *progr;
    struct block_t *block;
    struct vars_t *vars;
    struct ids_t *ids;
    int type;
    struct func_t *func;
    struct stmt_t *stmt;
    struct exp_t *exp;
};

%type <progr> progr
%type <block> block
%type <vars> vblock vars var ablock args
%type <ids> ids id
%type <type> type
%type <func> fblock funcs func
%type <stmt> cblock stmts stmt
%type <exp> expl exps exp

%%

progr:
    PROGR ID ';' block '.'  { $$ = mk(progr); $$->name = $2; $$->block = $4; run_progr($$); }
    ;

block:
    vblock fblock cblock    { $$ = mk(block); $$->vars = $1; $$->funcs = $2; $$->stmts = $3; }
    ;

vblock:                     { $$ = 0; }
    | VAR vars              { $$ = $2; }
    ;

vars:                       { $$ = 0; }
    | var ';' vars          { $$ = $1; $$->next = $3; }
    ;

var:
    ids ':' type            { $$ = mk(vars); $$->ids = $1; $$->type = $3; }
    ;

ids:
    id                      { $$ = $1; }
    | id ',' ids            { $$ = $1 ; $$->next = $3; }
    ;

id:
    ID                      { $$ = mk(ids); $$->id = $1; }
    ;

type:
    S_INTEGER               { $$ = tp_integer; }
    | S_DOUBLE              { $$ = tp_double; }
    | S_STRING              { $$ = tp_string; }
    ;

fblock:
    funcs                   { $$ = $1; }
    ;

funcs:                      { $$ = 0; }
    | func funcs            { $$ = $1; $$->next = $2; }
    ;

func:
    PROC ID ablock ';' block ';'
                            { $$ = mk(func); $$->name = $2; $$->args = $3; $$->type = tp_void; $$->block = $5; }
    | FUNC ID ablock ':' type ';' block ';'
                            { $$ = mk(func); $$->name = $2; $$->args = $3; $$->type = $5; $$->block = $7; }
    ;

ablock:                     { $$ = 0; }
    | '(' args ')'          { $$ = $2; }
    ;

args: var                   { $$ = $1; }
    | var ';' args          { $$ = $1; $$->next = $3; }
    ;

cblock:
    BEG stmts END           { $$ = $2; }
    ;

stmts:                      { $$ = 0; }
    | stmt stmts            { $$ = $1; $$->next = $2; }
    ;

stmt:
    cblock ';'                      { $$ = mk(stmt); $$->type = st_block; $$->block.stmt = $1; }
    | ID ATTR exp ';'               { $$ = mk(stmt); $$->type = st_attr; $$->attr.id = $1; $$->attr.exp = $3; }
    | ID expl ';'                   { $$ = mk(stmt); $$->type = st_func; $$->func.name = $1; $$->func.exp = $2; }
    | WHILE exp DO stmt             { $$ = mk(stmt); $$->type = st_while; $$->while_.exp = $2; $$->while_.stmt = $4; }
    | IF exp THEN stmt %prec IFX    { mkif($$, $2, $4, 0); }
    | IF exp THEN stmt ELSE stmt    { mkif($$, $2, $4, $6); }
    ;

expl:
    '(' ')'                 { $$ = 0; }
    | '(' exps ')'          { $$ = $2; }
    ;

exps: exp                   { $$ = $1; }
    | exp ',' exps          { $$ = $1; $$->next = $3; }
    ;

exp:
    ID                      { $$ = mk(exp); $$->type = ex_var; $$->var = $1; }
    | INTEGER               { $$ = mk(exp); $$->type = ex_integer; $$->num_i = $1; }
    | DOUBLE                { $$ = mk(exp); $$->type = ex_double; $$->num_d = $1; }
    | STRING                { $$ = mk(exp); $$->type = ex_string; $$->str = $1; }
    | ID expl               { $$ = mk(exp); $$->type = ex_func; $$->func.name = $1; $$->func.exp = $2; }
    | exp 'o' exp           { mkop($$, 'o', $1, $3); }
    | exp 'a' exp           { mkop($$, 'a', $1, $3); }
    | exp '=' exp           { mkop($$, '=', $1, $3); }
    | exp '<' exp           { mkop($$, '<', $1, $3); }
    | exp '>' exp           { mkop($$, '>', $1, $3); }
    | exp '+' exp           { mkop($$, '+', $1, $3); }
    | exp '-' exp           { mkop($$, '-', $1, $3); }
    | exp '*' exp           { mkop($$, '*', $1, $3); }
    | exp '/' exp           { mkop($$, '/', $1, $3); }
    | exp '%' exp           { mkop($$, '%', $1, $3); }
    | '!' exp               { mkop($$, '!', $2, 0); $$->type = ex_op1; }
    | '-' exp %prec NEG     { mkop($$, 'n', $2, 0); $$->type = ex_op1; }
    | '(' exp ')'           { $$ = $2; }
    ;

%%

int yywrap(void)
{
    return 1;
}

void yyerror(char *fmt, ... )
{
    va_list args;
    va_start(args, fmt);
    printf("line %d: ", yylineno);
    vprintf(fmt, args);
    printf("\n");
    va_end(args);
}

int main(int argc, char *argv[])
{
    yyin = fopen(argv[1], "r");
    yyparse();
    return 0;
}
