#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "env.h"

#define foreach(x) for (; x; x = x->next)

/* current environment */

struct env_t *cur = 0;

/* environment manipulation */

void push_env(struct vars_t *vars, struct func_t *func)
{
    struct env_t *env = malloc(sizeof(struct env_t));

    env->func = func;
    env->var = 0;
    env->next = cur;
    cur = env;

    make_vars(vars);
}

void pop_env()
{
    cur = cur->next;
}

/* variable functions */

void make_vars(struct vars_t *vars)
{
    struct ids_t *ids;

    foreach (vars) {
        ids = vars->ids;

        foreach (ids) {
            new_var(ids->id, vars->type);
        }
    }
}

void new_var(char *id, enum e_types type)
{
    struct var_t *var = malloc(sizeof(struct var_t));

    var->id = id;
    var->val.type = type;
    var->next = cur->var;

    cur->var = var;
}

struct var_t *find_var(char *id)
{
    struct env_t *env = cur;
    struct var_t *var;

    foreach (env) {
        var = env->var;
        foreach (var)
            if (!strcmp(var->id, id))
                return var;
    }

    fprintf(stderr, "unknown variable: %s\n", id);
    exit(1);
}

void set_var(char *id, struct val_t *val)
{
    struct var_t *var = find_var(id);

    if (var->val.type == tp_double && val->type == tp_integer)
        to_double(val);

    if (var->val.type == tp_integer && val->type == tp_double)
        to_integer(val);

    if (var->val.type != val->type)
        type_err("incompatible types");

    var->val = *val;
}

void get_var(char *id, struct val_t *val)
{
    struct var_t *var = find_var(id);

    *val = var->val;
}

/* function functions */

struct func_t *find_func(char *name)
{
    struct env_t *env = cur;
    struct func_t *func;

    foreach (env) {
        func = env->func;
        foreach (func)
            if (!strcmp(func->name, name))
                return func;
    }

    fprintf(stderr, "unknown function: %s\n", name);
    exit(1);
}
