%{

#include <stdlib.h>
#include "y.tab.h"

void yyerror(char *, ...);

char buf[1024], *s;

%}

%option yylineno

%x STR

%%

\/\*([^*]|(\*+[^*/]))*\*+\/  ;
[ \r\t\n]+          ;

"program"           return PROGR;

"var"               return VAR;

"integer"           return S_INTEGER;
"double"            return S_DOUBLE;
"string"            return S_STRING;

"procedure"         return PROC;
"function"          return FUNC;

"begin"             return BEG;
"end"               return END;

":="                return ATTR;

"while"             return WHILE;
"do"                return DO;
"if"                return IF;
"then"              return THEN;
"else"              return ELSE;

[-+/*%]             return *yytext;
[<>=!]|and|or       return tolower(*yytext);
[.,:;()]            return *yytext;

[_a-z][_a-z0-9]*    {
                        yylval.id = malloc(yyleng+1);
                        strcpy(yylval.id, yytext);
                        return ID;
                    }

[0-9]+              {
                        yylval.num_i = atoi(yytext);
                        return INTEGER;
                    }

[0-9]+\.[0-9]+      {
                        yylval.num_d = atof(yytext);
                        return DOUBLE;
                    }

\'                  { s = buf; BEGIN(STR); }

<STR>{
    \'              {
                        BEGIN(INITIAL);
                        *s = 0;
                        yylval.str = malloc(strlen(buf));
                        strcpy(yylval.str, buf);
                        return STRING;
                    }

    \\n             *s++ = '\n';
    \\t             *s++ = '\t';
    \\.             *s++ = yytext[1];
    .               *s++ = *yytext;
}

.                   { yyerror("unknown character %c", *yytext); exit(0); }

%%
