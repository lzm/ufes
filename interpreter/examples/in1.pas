program hello;

function hello: string;
begin
    ret := 'hello world';
end;

begin
    println(hello());
end.
