program demo;

var
    y, n: integer;
    x: double;
    z: string;

function fib(n: integer) : integer;
begin
    if n > 1 then
        ret := fib(n-1)+fib(n-2);
    else
        ret := n;
end;

procedure p2(x, y: double);
begin
    print(x);
    print(' ');
    print(y);
end;

begin
    n := 1;
    while n < 25 do
    begin
        println('fib ', n, ' = ', fib(n));
        n := n+1;
    end;

    z := 'zeh\n';
    print(z);

    x := 1.1;
    while x < 10 do
    begin
        println(x, 1, 2, 'maneh', 1.32423423432432);
        x := x + 1;
    end;

    p2(-23434422, 34524423);

    y := 123*12>2;

    if y and 3>9 or (4%2=0) then
        println(x);
    else
        println(0);

    begin
        println(2);
        println(3);
    end;

end.
