program factorial;

var
    n: integer;

function fac(n: integer) : integer;
begin
    if n > 0 then
        ret := n * fac(n-1);
    else
        ret := 1;
end;

procedure recursivo(max: integer);

    procedure pr_int(n, f: integer);
    begin
        println(n, '! = ', f);
    end;

begin
    n := 0;
    while n < max do
    begin
        pr_int(n, fac(n));
        n := n+1;
    end;
end;


procedure iterativo(max: integer);

var
    x: double;
    k: integer;

    procedure pr_dbl(n: integer; f: double);
    begin
        println(n, '! = ', f);
    end;

begin
    n := 0;
    while n < max do
    begin
        x := 1.0;
        k := 0;

        while k < n do
        begin
            k := k + 1;
            x := x * k;
        end;

        pr_dbl(n, x);

        n := n+1;
    end;
end;

begin
    println('recursivo, integer');
    recursivo(12);

    println();

    println('iterativo, double');
    iterativo(12);
end.
