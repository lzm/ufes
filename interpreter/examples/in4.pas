program primes;

var
    n, k: integer;

begin
    print(2);

    n := 3;
    while n < 3000 do
    begin
        k := 3;

        while !(k*k > n) and n % k do
        begin
            k := k+2;
        end;

        if k*k > n then
        begin
            print(' ', n);
        end;

        n := n+2;
    end;

    println();
end.
