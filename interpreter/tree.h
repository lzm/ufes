#ifndef TREE_H
#define TREE_H

#include "val.h"

/* syntax tree */

enum e_exps { ex_integer, ex_double, ex_string, ex_var, ex_op, ex_op1, ex_func };

struct opt_t {
    char op;
    struct exp_t *left, *right;
};

struct call_t {
    char *name;
    struct exp_t *exp;
};

struct exp_t {
    enum e_exps type;

    union {
        int num_i;
        double num_d;
        char *str;
        char *var;
        struct opt_t op;
        struct call_t func;
    };

    struct exp_t *next;
};

enum e_stmts { st_block, st_attr, st_func, st_if, st_while };

struct stmt_t {
    enum e_stmts type;

    union {
        struct {
            struct stmt_t *stmt;
        } block;
        struct {
            char *id;
            struct exp_t *exp;
        } attr;
        struct {
            struct exp_t *exp;
        } print;
        struct call_t func;
        struct {
            struct exp_t *exp;
            struct stmt_t *then, *else_;
        } if_;
        struct {
            struct exp_t *exp;
            struct stmt_t *stmt;
        } while_;
    };

    struct stmt_t *next;
};

struct vars_t {
    enum e_types type;
    struct ids_t *ids;
    struct vars_t *next;
};

struct func_t {
    char *name;
    enum e_types type;
    struct vars_t *args;
    struct block_t *block;
    struct func_t *next;
};

struct ids_t {
    char *id;
    struct ids_t *next;
};

struct block_t {
    struct vars_t *vars;
    struct func_t *funcs;
    struct stmt_t *stmts;
};

struct progr_t {
    char *name;
    struct block_t *block;
};

#endif
