#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "run.h"
#include "env.h"

#define max(x,y) (x>y?x:y)
#define foreach(x) for (; x; x = x->next)

/* syntax tree functions */

void eval_op1(struct opt_t *op, struct val_t *val)
{
    eval_exp(op->left, val);

    if (op->op == '!') {
        if (val->type != tp_integer) type_err("operator !");
        val->num_i = !val->num_i;
        return;
    }

    if (op->op == 'n') {
        switch (val->type) {
            case tp_integer: val->num_i = -val->num_i; return;
            case tp_double: val->num_d = -val->num_d; return;
            default: type_err("operator -");
        }
    }
}

void eval_op(struct opt_t *op, struct val_t *val)
{
    struct val_t vl, vr;

    if (op->left)  eval_exp(op->left,  &vl);
    if (op->right) eval_exp(op->right, &vr);

    switch (op->op) {
        case 'o': case 'a':
        case '%':
            if (vl.type != tp_integer || vr.type != tp_integer)
                type_err("operators and, or, and %%");

            val->type = tp_integer;
            break;

        case '<': case '>': case '=':
        case '+': case '-':
        case '*': case '/':
            switch (max(vr.type, vl.type)) {
                case tp_string:
                case tp_void:
                    type_err("operators +-*/");

                case tp_double:
                    to_double(&vl);
                    to_double(&vr);
                    val->type = tp_double;
                    break;

                case tp_integer:
                    val->type = tp_integer;
            }

            if (op->op == '<' || op->op == '>' || op->op == '=')
                val->type = tp_integer;

            break;
    }

    switch (op->op) {
        case 'o': val->num_i = vl.num_i || vr.num_i; return;
        case 'a': val->num_i = vl.num_i && vr.num_i; return;
        case '=': val->num_i = vl.num_i == vr.num_i; return;
        case '%': val->num_i = vl.num_i % vr.num_i; return;
    }

    #define morph(v) do \
    switch (op->op) { \
        case '<': val->num_i = vl.v < vr.v; val->type = tp_integer; return; \
        case '>': val->num_i = vl.v > vr.v; val->type = tp_integer; return; \
        case '+': val->v = vl.v + vr.v; return; \
        case '-': val->v = vl.v - vr.v; return; \
        case '*': val->v = vl.v * vr.v; return; \
        case '/': val->v = vl.v / vr.v; return; \
    } while (0)

    if (val->type == tp_integer) morph(num_i);
    if (val->type == tp_double) morph(num_d);
}

void call_func(struct func_t *func, struct exp_t *exp, struct val_t *val)
{
    struct vars_t *vars;
    struct ids_t *ids;
    struct val_t tmp;

    push_env(0, 0);

    vars = func->args;
    foreach (vars) {
        ids = vars->ids;

        foreach (ids) {
            if (!exp) {
                fprintf(stderr, "%s: not enough args\n", func->name);
                exit(1);
            }
            eval_exp(exp, &tmp);
            exp = exp->next;

            new_var(ids->id, vars->type);
            set_var(ids->id, &tmp);
        }
    }

    new_var("ret", func->type);

    run_block(func->block);

    if (val) get_var("ret", val);

    pop_env();
}

void eval_func(struct call_t *call, struct val_t *val)
{
    struct val_t tmp;
    struct exp_t *exp;

    int func = 0;
    if (!strcmp(call->name, "print")) func = 1;
    if (!strcmp(call->name, "println")) func = 2;

    if (func) {
        exp = call->exp;
        foreach (exp) {
            eval_exp(exp, &tmp);

            switch (tmp.type) {
                case tp_void:    printf("<void>");        break;
                case tp_integer: printf("%d", tmp.num_i); break;
                case tp_double:  printf("%g", tmp.num_d); break;
                case tp_string:  printf("%s", tmp.str);   break;
            }
        }
        if (func == 2) puts("");
        if (val) val->type = tp_void;

        return;
    }

    call_func(find_func(call->name), call->exp, val);
}

void eval_exp(struct exp_t *exp, struct val_t *val)
{
    switch (exp->type) {
        case ex_integer:
            val->type = tp_integer;
            val->num_i = exp->num_i;
            break;

        case ex_double:
            val->type = tp_double;
            val->num_d = exp->num_d;
            break;

        case ex_string:
            val->type = tp_string;
            val->str = exp->str;
            break;

        case ex_var:
            get_var(exp->var, val);
            break;

        case ex_op:
            eval_op(&exp->op, val);
            break;

        case ex_op1:
            eval_op1(&exp->op, val);
            break;

        case ex_func:
            eval_func(&exp->func, val);
            break;
    }
}

void run_stmt(struct stmt_t *stmt)
{
    struct val_t val;

    switch (stmt->type) {

        case st_block:
            run_stmts(stmt->block.stmt);
            break;

        case st_attr:
            eval_exp(stmt->attr.exp, &val);
            set_var(stmt->attr.id, &val);
            break;

        case st_func:
            eval_func(&stmt->func, 0);
            break;

        case st_if:
            eval_exp(stmt->if_.exp, &val);

            if (val.type != tp_integer)
                type_err("if expression requires an integer condition");

            if (val.num_i) run_stmt(stmt->if_.then);
            else if (stmt->if_.else_) run_stmt(stmt->if_.else_);

            break;

        case st_while:
            for (;;) {
                eval_exp(stmt->while_.exp, &val);

                if (val.type != tp_integer)
                    type_err("while expresson requires an integer condition");

                if (!val.num_i) break;

                run_stmt(stmt->while_.stmt);
            }
            break;
    }
}

void run_stmts(struct stmt_t *stmt)
{
    foreach (stmt) {
        run_stmt(stmt);
    }
}

void run_block(struct block_t *block)
{
    push_env(block->vars, block->funcs);

    run_stmts(block->stmts);

    pop_env();
}

void run_progr(struct progr_t *progr)
{
    printf("> running \"%s\"\n", progr->name);

    run_block(progr->block);
}
