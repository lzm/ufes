#include <stdio.h>
#include <stdlib.h>

#include "val.h"

void to_integer(struct val_t *val)
{
    switch (val->type) {
        case tp_integer:
            break;

        case tp_double:
            val->num_i = val->num_d;
            break;

        case tp_string:
            val->num_i = 0;
            break;
    }

    val->type = tp_integer;
}

void to_double(struct val_t *val)
{
    switch (val->type) {
        case tp_integer:
            val->num_d = val->num_i;
            break;

        case tp_double:
            break;

        case tp_string:
            val->num_d = 0;
            break;
    }

    val->type = tp_double;
}

void type_err(char *s)
{
    fprintf(stderr, "type mismatch: %s\n", s);
    exit(1);
}
