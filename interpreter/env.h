#ifndef ENV_H
#define ENV_H

#include "val.h"
#include "tree.h"

struct var_t {
    char *id;
    struct val_t val;
    struct var_t *next;
};

struct env_t {
    struct var_t *var;
    struct func_t *func;
    struct env_t *next;
};

void push_env(struct vars_t *vars, struct func_t *func);
void pop_env();

void make_vars(struct vars_t *vars);

void new_var(char *id, enum e_types type);
void set_var(char *id, struct val_t *val);
void get_var(char *id, struct val_t *val);

struct func_t *find_func(char *name);

#endif
