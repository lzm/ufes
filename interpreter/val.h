#ifndef VAL_H
#define VAL_H

enum e_types { tp_void, tp_integer, tp_double, tp_string };

struct val_t {
    enum e_types type;

    union {
        int num_i;
        double num_d;
        char *str;
    };
};

/* coercions */

void to_integer(struct val_t *val);

void to_double(struct val_t *val);

void type_err(char *s);

#endif
