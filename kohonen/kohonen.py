#!/usr/bin/env python

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import sys, time, random, math

un = 30
xn = 100
dn = 3

a = 0.1
t = 1.0
s = 5.0

xs = [[random.uniform(-1,1) for d in range(dn)] for x in range(xn)]
us = [[random.uniform(-1,1) for d in range(dn)] for x in range(un)]
vs = [[x for x in u] for u in us]

mulv = lambda u,k: [x*k for x in u]
sumv = lambda u,v: [x+y for x,y in zip(u,v)]
difv = lambda u,v: [x-y for x,y in zip(u,v)]
norm = lambda u: dot(u,u)**0.5
dot  = lambda u,v: sum([x*y for x,y in zip(u,v)])
dist = lambda u,v: norm(difv(u,v))
angl = lambda u,v: -dot(u,v)/norm(u)/norm(v)

def train(ss, sim, t):
   for x in xs:
      i = min([(sim(x, ss[i]), i) for i in range(un)])[1]
      for u,j in zip(ss,range(un)):
         #if i!=j: continue
         alpha = a/t
         dst = min(i-j, 1000+un-(i-j))
         phi = 2**(-dst**2/(s/t)**2)
         ss[j] = sumv(ss[j], mulv(difv(x, ss[j]), alpha*phi))

def step():
   global t, us, vs, ax
   print t, int(t)
   for x in range(int(t)):
      t += 0.01
      train(us, dist, t) #1.5) #math.log(t))
      #train(vs, angl, t)
   draw()
   if int(t) == 14:
       glutIdleFunc(None)
   ax += 0.5

ox = 0
oy = 0
ax = 0
ay = 0

def draw():
   glClear(GL_COLOR_BUFFER_BIT)
   glLoadIdentity()
   glTranslatef(0, 0, -2.5)

   glRotatef(-ax, 0, 1, 0)
   glRotatef(-ay, 1, 0, 0)

   plot = dn == 2 and glVertex2fv or glVertex3fv

   glColor3f(1, 1, 1)
   glBegin(GL_POINTS)
   for x in xs:
      plot(x)
   glEnd()

   glColor4f(1, 1, 0, 0.4)
   glBegin(GL_LINE_STRIP)
   for u in us:
      plot(u)
   glEnd()

   #glColor4f(1, 0, 1, 0.4)
   #glBegin(GL_LINE_STRIP)
   #for v in vs:
   #   plot(v)
   #glEnd()

   glutSwapBuffers()

def resize(w, h):
   glViewport(0, 0, w, h)
   glMatrixMode(GL_PROJECTION)
   glLoadIdentity()
   gluPerspective(45.0, float(w)/float(h), 0.1, 100.0)
   glMatrixMode(GL_MODELVIEW)

def key(k, n1, n2):
   if k == '\033':
      glutDestroyWindow(win)
      sys.exit()

   if k == ' ':
      step()

def mouse(b, s, x, y):
   global ox, oy
   ox = x
   oy = y

def move(x, y):
   global ax, ay, ox, oy
   ax += ox - x
   ay += oy - y
   ox = x
   oy = y
   draw()

def initgl():
   glClearColor(0.0, 0.0, 0.0, 0.0)
   glShadeModel(GL_SMOOTH)
   glEnable(GL_LINE_SMOOTH)
   glEnable(GL_BLEND)
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
   glLineWidth(0.6)

if __name__ == '__main__':
   glutInit(sys.argv)
   glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA)
   glutInitWindowSize(600, 400)
   win = glutCreateWindow("kohonen map")
   glutDisplayFunc(draw)
   glutReshapeFunc(resize)
   glutKeyboardFunc(key)
   glutMouseFunc(mouse)
   glutMotionFunc(move)
   glutIdleFunc(step)
   initgl()
   glutMainLoop()
